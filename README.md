# How to start a Sylius project:

## 1. Download "Docker" repository and extract it into your project directory (example: Home/Projects/<your_project>). It should look like this:

![](https://i.imgur.com/MMft7JA.png)

## 2. Next you need to download project itself and extract it to the same folder. The name of folder should be "detales"

![](https://i.imgur.com/XkytaFo.png)

## 3. From your root docker directory (example: Home/Projects/<your_project>), you need to open your CLI terminal and type in ```docker-compose build``` . After the build is done docker containers should be started. You can check if they are running by opening new terminal tab and typing ```docker ps```. There should be four containers:

![](https://i.imgur.com/PPViKiL.png)

If all of them are there, you can now type in ```docker exec -it automobiliudetales_php_1 bash``` and your container will open

## 4. Type ```composer update```. If everything goes well, you will be ready to launch project.

## 5. Type ```php bin/console``` if you see Sylius command list, then skip to step 8, otherwise type ```php bin/console sylius:install```.

## 6. After installation is finished, type ```php bin/console sylius:fixtures:load```

## 7. After fixtures are loaded, type ```php bin/console sylius:install:assets``` to install all assets.

## 8. After everything is done, head to your browser and type in [http://127.0.0.1:8080/](http://127.0.0.1:8080/)

It should look like this:
![](https://i.imgur.com/7JNojOB.png)

## 9. Final step is to download database dump from repository, open new tab in your CLI terminal and type in ```docker exec -it automobiliudetales_mysql_1 mysql -uroot -proot sylius < dbdump.sql